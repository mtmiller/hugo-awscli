Docker container to build a static [Hugo] website and deploy it to [S3].

Based on the GitLab Pages Hugo container at [pages/hugo].

[hugo]: https://gohugo.io
[s3]: https://aws.amazon.com/s3/
[pages/hugo]: https://gitlab.com/pages/hugo
