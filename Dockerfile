## SPDX-License-Identifier: MIT
## Copyright (C) 2019 Mike Miller
ARG HUGO=hugo
FROM registry.gitlab.com/pages/hugo/$HUGO:latest
RUN apk add --update --no-cache python3 && \
    rm -rf /var/cache/apk/*
RUN pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir --upgrade awscli
